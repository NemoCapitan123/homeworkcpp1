#include <vector>
#include <algorithm>
#include <iostream>


int main() {
	std::vector<int> ReportsOfErr(10);

	srand(time(0u));
	std::generate(ReportsOfErr.begin(), ReportsOfErr.end(), []()->int { return float(rand()) / float(RAND_MAX) * 10; });

	for (auto i : ReportsOfErr) {
		std::cout << i << " ";
	}
}